# BlinkPay : Send and Recieve Money Across Africa

Powered by Solana, BlinkPay helps you conveniently send and recieve money in an instant ⚡ without any hassle. You only need to simply connect your Wallet to get started.


## Getting Started

First, clone the repo, `yarn` and run the development server:

```bash
npm run dev
# or
yarn dev
```
